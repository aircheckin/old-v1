<?php
check_user_access('admin');
global $user;

if (!empty($_REQUEST['contract_id'])) {
	$contract_id = (int)$_REQUEST['contract_id'];

	$contract_data = $db->getRow("
	SELECT o.id as owner_id, c.address FROM owners_contracts as c
	LEFT JOIN owners AS o ON o.user_id = c.user_id
	WHERE c.id=?i
	", $contract_id);

	if ($contract_data) {
		$db->query("INSERT INTO aparts SET name='Новый апартамент', owner_id=?i, address=?s",
			$contract_data['owner_id'], $contract_data['address']);

		$apartment_id = $db->insertId();

		$db->query('UPDATE owners_contracts SET status=?i, apartment_id=?i WHERE id=?i',
			OWNER_CONTRACT_STATUS['accepted'], $apartment_id, $contract_id);

		json_answer('Договор подтверждён, создан новый апартамент');
	}

}

json_answer('error', 'Ошибка обновления договора');
