<?
function getRadiosHtml()
{
	$filters = '';
	$filterStatuses = [
		'checking' => 'Активные',
		'accepted' => 'Подтверждённые',
		'rejected' => 'Отклонённые',
		'all'      => 'Все',
	];

	foreach ($filterStatuses as $name => $title) {
		if ($name == $_GET['filter']) {
			$filters .= '
			<label class="btn active js_filter_radio" data-filter="' . $name . '">
				<input type="radio" name="filter" checked>' .
						$title
						. '
			</label>';
		} else {
			$filters .= '
			<label class="btn js_filter_radio" data-filter="' . $name . '">
				<input type="radio" name="filter">' . $title . '
			</label>';
		}
	}


	return $filters;
}

load_tpl('/views/admin/template/header.tpl');
load_tpl('/views/admin/owners/contracts/list.tpl', ['radioFilters' => getRadiosHtml()]);
