<?php
check_user_access('admin');

if (!empty($_REQUEST['contract_id'])) {
	$contract_id = (int)$_REQUEST['contract_id'];

	$db->query('UPDATE owners_contracts SET status=?i WHERE id=?i',
		OWNER_CONTRACT_STATUS['rejected'], $contract_id);

	json_answer('Договор отклонён');
}

json_answer('error', 'Ошибка обновления договора');
