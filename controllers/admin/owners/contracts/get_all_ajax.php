<?
check_user_access('admin');
include_once ROOT.'/lib/owner_contracts.php';

$queryFilter = '';


if (isset($_GET['filter'])) {
	$filter = $_GET['filter'];

	if (isset(OWNER_CONTRACT_STATUS[$filter])) {
		$queryFilter = "WHERE oc.status = " . OWNER_CONTRACT_STATUS[$filter];
	} elseif ($filter === 'requests') {
		$requestStatuses = implode(', ', [
			OWNER_CONTRACT_STATUS['checking'],
			OWNER_CONTRACT_STATUS['rejected'],
		]);

		$queryFilter = "WHERE oc.status IN ($requestStatuses)";
	}
}

$contracts = $db->getAll("
SELECT oc.*, u.name, u.phone FROM owners_contracts as oc
LEFT JOIN users AS u ON u.id=user_id
$queryFilter
ORDER BY oc.updated_at DESC
");

foreach ($contracts as $key => $contract) {
	$contracts[$key]['name'] = "<span class='contract_id' data-id='{$contract['id']}'>{$contract['name']}</span>";
	$contracts[$key]['updated_at'] = date('Y.m.d H:i', strtotime($contract['updated_at']));
	$contracts[$key]['phone'] = '<a href="tel://'.$contract['phone'].'">' . $contract['phone'] .'</a>';
	$contracts[$key]['status'] = get_owner_contract_status_title($contract['status']);
}

if (empty($contracts)) {
	json_answer([]);
} else {
	json_answer($contracts);
}
