<?
check_user_access('admin');

if (!empty($_REQUEST['contract_id'])) {
	$contract_id = (int)$_REQUEST['contract_id'];
	$contract = $db->getRow("
SELECT oc.*, u.name, u.phone FROM owners_contracts as oc
LEFT JOIN users AS u ON u.id=user_id
WHERE oc.id=?i
", $contract_id);


	include_once ROOT . '/lib/owner_contracts.php';
	$contract['status_title'] = get_owner_contract_status_title($contract['status']);
	$contract['images'] = get_owner_contract_images($contract['id']);

	exit(json_html('/views/admin/owners/contracts/modal.tpl',
		['contract' => $contract]));
}
