<?
require_once ROOT . '/lib/messaging.php';
check_user_access('admin');

global $user;

$tpl_data = [];

if (!empty($_POST)) {
  if (isset($_POST['owner_user_id'])) {
		
		$owner = $db->getRow("SELECT * FROM owners WHERE user_id=?i", $_POST['owner_user_id']);
    $tpl_data['owner'] = $owner;

    if (isset($_POST['amount']) && isset($_POST['amount'])) {
			$amount = $_POST['amount']*100;
      $db->query("INSERT INTO owners_balance SET owner_id=?i, order_id=0, sum=?i, created_at=?i", $owner['id'], $amount, time());

      $tpl_data['message'] = 'Собственнику начислено ' . $_POST['amount'] . 'р. на баланс';
      global $user;
      $log->info($user['name']. ' начислил ' . $_POST['amount'] . 'р. на баланс собственнику №'.$owner['user_id']);
			
			
			send_tg($user['name']. ' начислил ' . $_POST['amount'] . 'р на баланс собственнику '.$owner['name']);
			
			// print_r($owner);
			

      load_tpl('/views/admin/template/header.tpl');
      load_tpl('/views/admin/add_owner_balance/search_owner.tpl', $tpl_data);
      load_tpl('/views/admin/template/footer.tpl');
      exit();
    }

    if (!empty($tpl_data['owner'])) {
			$tpl_data['owner']['balance'] = $db->getOne("SELECT SUM(sum) FROM owners_balance WHERE owner_id=?i AND payed_out=0", $owner['id']);
      load_tpl('/views/admin/template/header.tpl');
      load_tpl('/views/admin/add_owner_balance/add_owner_balance.tpl', $tpl_data);
      load_tpl('/views/admin/template/footer.tpl');

      exit();
    } else {
      $tpl_data['message'] = 'Собственник с таким номером не найден!';
    }
  }
}

load_tpl('/views/admin/template/header.tpl');
load_tpl('/views/admin/add_owner_balance/search_owner.tpl', $tpl_data);
load_tpl('/views/admin/template/footer.tpl');