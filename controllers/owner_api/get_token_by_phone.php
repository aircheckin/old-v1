<?php

require_once ROOT . '/lib/messaging.php';

$fields = [
	'phone' => format_phone7($_POST['phone']) ?? null,
	'code' => $_POST['code'] ?? null,
];

foreach ($fields as $k => $f) {
	if (empty($f)) {
		json_answer('error', 'Не корректное поле '.$k);
	}
}

$check_code = $db->getOne(
	"SELECT code FROM sms_check_codes WHERE phone=?s AND expiration_date>?i ORDER BY id DESC",
	$fields['phone'], time());

if ($check_code == $fields['code']) {
	$user = get_user_by_login($fields['phone']);

	if ($user) {
		json_answer('ok', ['token'=>generate_user_token($user['id'])]);
	} else {
		json_answer('error', 'Пользователь с таким номером телефона не найден');
	}

} else {
	json_answer('error', 'Не верный проверочный код, попробуйте ещё раз');
}

exit();
