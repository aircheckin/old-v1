<?php

use Intervention\Image\ImageManagerStatic as Image;

if (!empty($_POST['contract_id'])) {
	$contract_id = $db->getOne("SELECT id FROM owners_contracts WHERE id=?i",
		$_POST['contract_id']);

	if ($contract_id) {
		if (!empty($_FILES) && !empty($_FILES['files']) && !empty($_FILES['files']['name'])) {
			$files_dir = OWNERS_CONTRACTS_FILES_DIR . "$contract_id/images/";
			$thumbs_dir = $files_dir . 'thumbs/';

			if (!is_dir($thumbs_dir)) {
				if (!mkdir($thumbs_dir, 0755, true) && !is_dir($thumbs_dir)) {
					throw new \RuntimeException(sprintf('Directory "%s" was not created',
						$thumbs_dir));
				}
			}
			foreach ($_FILES['files']['name'] as $key => $file_name) {
				if (stripos($_FILES['files']['type'][$key], 'image') === false) {
					$errors[] = $file_name;
					continue;
				}

				$file_ext = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_filename = randomString(32) . '.' . $file_ext;

				$new_file_path = $files_dir . $new_filename;
				$new_thumb_file_path = $thumbs_dir . $new_filename;

				$success_uploaded = move_uploaded_file($_FILES['files']['tmp_name'][$key],
					$new_file_path);

				$image = Image::make($new_file_path)->resize(250, null, function ($constraint) {
					$constraint->aspectRatio();
				})->save($new_thumb_file_path, 50);

				if (!$success_uploaded) {
					$errors[] = $file_name;
				}
			}

			if (empty($errors)) {
				json_answer('ok', 'Файлы успешно загружены');
			} else {
				json_answer('error',
					['message' => 'Не удалось загрузить файлы', 'files' => $errors]);
			}
		}
	} else {
		json_answer('error', 'Договор с указанным id не найден');
	}
}


json_answer('error', 'Необходимо указать contract_id и массив фотографий files');
