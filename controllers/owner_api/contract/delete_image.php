<?php
if (!empty($_POST['filename']) && !empty($_POST['contract_id'])) {
	$contract_id = $db->getOne("SELECT id FROM owners_contracts WHERE id=?i AND user_id=?i",
		$_POST['contract_id'], $user['id']);

	if ($contract_id) {
		$filename = preg_replace('/\.\./', '', $_POST['filename']);

		$images_dir = OWNERS_CONTRACTS_FILES_DIR . $contract_id . '/images/';
		$file = $images_dir . '/' . $filename;
		$thumb = $images_dir . '/thumbs/' . $filename;

		if (is_file($file)) {
			unlink($file);
		}

		if (is_file($thumb)) {
			unlink($thumb);
		}

		json_answer('Файл удалён');
	} else {
		json_answer('Не найден контракт с указанным id');
	}

} else {
	json_answer('error', 'Необходимо указать поле filename');
}
