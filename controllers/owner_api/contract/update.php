<?php
require_once ROOT . '/lib/messaging.php';

if (!empty($_POST['contract_id'])) {
	$contract_id = $db->getOne("SELECT id FROM owners_contracts WHERE id=?i",
		$_POST['contract_id']);

	if (!$contract_id) {
		json_answer('error', 'Договор с таким id не найден');
	}


	$fields = [
		'passport_number',
		'egrn',
		'water_meter_hot',
		'water_meter_cold',
	];


	foreach ($fields as $f) {
		if (isset($_POST[$f])) {
			$update_fields[$f] = $_POST[$f];
		}
	}

	if (!empty($update_fields)) {
		$db->query("UPDATE owners_contracts SET ?u WHERE id=?i", $update_fields, $contract_id);
	}

	json_answer('ok',
		['contract_id' => $contract_id, 'message' => 'Обновлены данные договора']);

}

json_answer('Необходимо указать поле contract_id');
