<?php
$fields = "id,address,passport_number,egrn,water_meter_hot,water_meter_cold,status,created_at,updated_at";

$contracts = $db->getAll("SELECT $fields FROM owners_contracts WHERE user_id=?i", $user['id']);

if (!empty($contracts)) {
	include_once ROOT . '/lib/owner_contracts.php';


	foreach ($contracts as $key => $contract) {
		$images = get_owner_contract_images($contract['id']);
		$contracts[$key]['images'] = $images;
	}
} else {
	$contracts = [];
}

json_answer($contracts);
