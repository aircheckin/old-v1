<?php
json_answer('Функция не используется');

if (!empty($_POST['contract_id'])) {
	$contract = $db->getRow("SELECT * FROM owners_contracts WHERE id=?i AND user_id=?i",
		$_POST['contract_id'], $user['id']);

	if ($contract) {
		if ($contract['status'] === OWNER_CONTRACT_STATUS['accepted']) {
			$db->query("UPDATE owners_contracts WHERE id=?i AND status=?i SET status=?i",
				$_POST['contract_id'],
				OWNER_CONTRACT_STATUS['accepted'],
				OWNER_CONTRACT_STATUS['closed']);

			if ($db->affectedRows()) {
				json_answer('Договор закрыт');
			} else {
				json_answer('error', 'Ошибка закрытия договора');
			}
		} else {
			json_answer('error', 'Закрыть можно только договор со статусом accepted: ' .
								 OWNER_CONTRACT_STATUS['accepted']);
		}
	} else {
		json_answer('error', 'Не найден договор с указанным id');
	}
}

json_answer('error', 'Необходимо указать поле id');

