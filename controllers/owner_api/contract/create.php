<?php
require_once ROOT . '/lib/messaging.php';

const STATUS_CREATED = 1;
const STATUS_CHECKING = 2;
const STATUS_CANCELLED = 3;
const STATUS_BANNED = 4;

$required_fields = [
	'address' => $_POST['address'] ?? '',
];

foreach ($required_fields as $key => $f) {
	if (empty($f)) {
		json_answer('error', "Поле $key обязательно для заполнения");
	}
}

$fields = array_merge($required_fields, [
	'passport_number'  => $_POST['passport_number'] ?? '',
	'egrn'             => $_POST['egrn'] ?? '',
	'water_meter_hot'  => $_POST['water_meter_hot'] ?? '',
	'water_meter_cold' => $_POST['water_meter_cold'] ?? '',
	'user_id'          => $user['id'],
	'status'           => OWNER_CONTRACT_STATUS['created'],
]);

$db->query("INSERT INTO owners_contracts SET ?u", $fields);
$contract_id = $db->insertId();

if ($contract_id) {
	json_answer('ok',
		['contract_id' => $contract_id, 'message' => 'Создана заявка на подключение апартамента']);
}

json_answer('Не удалось создать заявку, попробуйте снова');
