<?php
if (!empty($_POST['apartment_id'])) {
	$apartment_id = $db->getOne("SELECT id FROM owners_contracts WHERE id=?i AND user_id=?i",
		$_POST['apartment_id'], $user['id']);

	if ($apartment_id) {
		$db->query("UPDATE owners_contracts SET status=2 WHERE id=?i", $apartment_id);
		json_answer('Апартамент отправлен на проверку');
	} else {
		json_answer('Не найден апартамент с указанным id');
	}
} else {
	json_answer('Необходимо указать apartment_id');
}
