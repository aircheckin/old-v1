<?php
if (!empty($_POST['contract_id'])) {
	$contract = $db->getRow("SELECT * FROM owners_contracts WHERE id=?i AND user_id=?i",
		$_POST['contract_id'], $user['id']);

	if ($contract) {
		$canStatuses = [
			OWNER_CONTRACT_STATUS['created'],
			OWNER_CONTRACT_STATUS['rejected'],
			OWNER_CONTRACT_STATUS['banned']
		];

		$canDeleted = in_array($contract['status'], $canStatuses, false);

		if ($canDeleted) {
			$db->query("DELETE FROM owners_contracts WHERE id=?i", $contract['id']);

			if ($db->affectedRows()) {
				$files_dir = OWNERS_CONTRACTS_FILES_DIR . $contract['id'];
				rrmdir($files_dir);

				json_answer('Договор удалён');
			} else {
				json_answer('error', 'Ошибка удаления договора');
			}
		} else {
			json_answer('error',
				'Удалить можно только если статус равен ' . implode(', ', $canStatuses));
		}
	} else {
		json_answer('error', 'Не найден договор с указанным id');
	}
} else {
	json_answer('error', 'Необходимо указать поле contract_id');
}


