<?php
if (!empty($_GET['id'])) {
	$fields = "id,address,passport_number,egrn,water_meter_hot,water_meter_cold,status,created_at,updated_at";

	$contract = $db->getRow("
		SELECT $fields
		FROM owners_contracts WHERE id=?i AND user_id=?i
		", $_GET['id'], $user['id']);

	if ($contract) {
		include_once ROOT . '/lib/owner_contracts.php';

		$images = get_owner_contract_images($contract['id']);
		$contract['images'] = $images;

		json_answer($contract);
	} else {
		json_answer('error', 'Апартамент с указанным id не найден');
	}
} else {
	json_answer('error', 'Необходимо указать поле id');
}


