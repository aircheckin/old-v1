<?php

require_once ROOT . '/lib/messaging.php';

if (!empty($_POST['phone'])) {
	$phone = format_phone7($_POST['phone']);
	$user = get_user_by_login($phone);

	if (!$phone) {
		json_answer('error', 'Укажите действительный номер телефона');
	}

	if (!$user) {
		json_answer('error', 'Пользователь с таким номером телефона не найден');
	}

	$sms_timeout = 60;

	if (check_form_spam('send_auth_sms', $phone, $sms_timeout)) {
		$check_code = rand(1000, 9999);
		$expiration_date = time() + 60 * 10;
		$db->query("INSERT INTO sms_check_codes SET expiration_date=?i, code=?i, phone=?s",
			$expiration_date, $check_code, $phone);

		$res = send_sms($phone, 'Apartservice код авторизации: ' . $check_code);

		if ($res->status == 'OK') {
			json_answer('Отправлено SMS с кодом');
		} else {
			json_answer('error', [
				'message' => $res->message ?? '',
				'code'    => $res->sms_status_code
							 ?? ''
			]);
		}

	} else {
		json_answer('error', 'Повторите попытку через минуту');
	}
} else {
	json_answer('error', 'Phone not recieved');
}

exit();
