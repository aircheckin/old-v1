<?php
require_once ROOT . '/lib/messaging.php';

$fullName = implode(' ', [
	'surname'     => $_POST['surname'] ?? '',
	'name'        => $_POST['name'] ?? '',
	'middle_name' => $_POST['middle_name'] ?? '',
]);

$fields = [
	'name'       => $fullName ?? '',
	'birth_date' => $_POST['birth_date'] ?? '',
	'phone'      => format_phone7($_POST['phone']) ?? '',
	'email'      => $_POST['email'] ?? '',
	'password'   => $_POST['password'] ?? '',
];

foreach ($fields as $key => $f) {
	if (empty($f)) {
		json_answer('error', "Поле $key обязательно для заполнения");
	}
}

$existUser = $db->getOne(
	"SELECT id FROM users WHERE login=?s", $fields['phone']);

if ($existUser) {
	json_answer('error', 'Пользователь с таким номером телефона уже зарегистрирован');
}

$user = add_user([
	'login'        => $fields['phone'],
	'email'        => $fields['email'],
	'phone'        => $fields['phone'],
	'name'         => $fields['name'],
	'password'     => create_password_hash($fields['password']),
	'access_level' => 10,
]);

if (!empty($user)) {
	$owner = add_owner(['user_id' => $user['id'], 'name' => $user['name']]);

	if (!empty($owner)) {
		$response = [
			'user'    => $owner,
			'message' => 'Создан пользователь, отправлен код верификации на номер телефона'
		];
	}

	send_tg("<b>Зарегистрировался новый собственник</b>
{$fields['name']}
+{$fields['phone']}
{$fields['email']}");

	json_answer($owner);
}

json_answer('error', 'Не удалось создать пользователя');
