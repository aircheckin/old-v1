<div class="row"><div class="col-sm-12"><h4>Начисление баланса собственнику</h4></div></div><hr>
<div class="row mb-4">
	<div class="col-md-8">
		<form name="add_balance_form" action="/admin/add_owner_balance" method="POST">
			<div class="form-group row">
				<div class="col-sm-12">
					<input class="form-control" required id="input_owner_user_id" name="owner_user_id" placeholder="№ собственника" value="<?=$owner_user_id?>" type="number"/>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-12">
					<button class="btn btn-warning">Найти собственника
					</button>
				</div>
			</div>
		</form>
		<?if (isset($message)) {?>
			<div class="row">
				<div class="col">
					<p><?=$message?></p>
				</div>
			</div>
			<?}?>
		</div>
	</div>