<div class="modal-header">
	<h5 class="modal-title" id="order_modal_title">Заявка на подключение апартамента</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="modal-body">
	Номер заявки: <?=$contract['id']?><br>
	Адрес: <?=$contract['address']?><br>
	Собственник: <?=$contract['name']?><br>
	Телефон: <a href="tel://<?=$contract['phone']?>"><?=$contract['phone']?></a><br>
	Паспорт: <?=$contract['passport_number']?><br>
	ЕГРН: <?=$contract['egrn']?><br>
	Счётчик горячей воды: <?=$contract['water_meter_hot']?><br>
	Счётчик холодной воды: <?=$contract['water_meter_cold']?>
	<hr>
	Дата создания: <?=$contract['created_at']?><br>
	Дата обновления: <?=$contract['updated_at']?><br>
	Статус: <?=$contract['status_title']?>
	<?php if ($contract['apartment_id']) {?>
	<br><br>
	<a href="/admin/aparts#/edit/<?=$contract['apartment_id']?>">Перейти в апартамент</a>
	<?php } ?>
	<?php
  $images = '';
  foreach($contract['images'] as $image) {
    $imagename = basename($image['full']);
    $images .= "<a href='{$image["full"]}' data-ngthumb='{$image["thumb"]}'></a>";
  }

  if (!empty($images)) {
	?>
	<hr>
	<div id="my_nanogallery2">
		<?= $images?>
	</div>
  <?php } ?>
	<?php if ($contract['status'] != OWNER_CONTRACT_STATUS['accepted']) {?>
	<hr>
	<a href="#" class="btn btn-success" id="modal_accept_button" data-id="<?=$contract['id']?>">Подтвердить
		подключение</a>
	<?php if ($contract['status'] != OWNER_CONTRACT_STATUS['rejected'] ) { ?>
	<a href="#" class="btn btn-danger float-right" id="modal_reject_button" data-id="<?=$contract['id']?>">Отклонить</a>
	<?php }} ?>
</div>
