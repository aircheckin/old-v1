<!--<link rel="stylesheet" href="<?=WEB_ROOT?>/public/css/nanogallery2.min.css">-->
<link rel="stylesheet" href="<?=WEB_ROOT?>/public/css/nanogallery2.woff.min.css">

<div class="row">
	<div class="col-sm-12"><h4>Заявки на подключение апартамента</h4></div>
</div>
<hr>
<div class="btn-group radios-without-style mb-3" data-toggle="buttons">
	<?=$radioFilters?>
</div>

<table id="owner_contracts" class="table table-hover table-striped table-sm table-bordered">
	<div class="row">
	</div>
	<thead>
	<tr>
		<th>№</th>
		<th>Собственник</th>
		<th>Телефон</th>
		<th>Адрес</th>
		<th>Статус</th>
		<th>Дата обновления</th>
	</tr>
	</thead>
	<tbody>
	</tbody>
	<tfoot>
	</tfoot>
</table>

<!-- Modal -->
<div class="modal fade" id="contract_modal" tabindex="-1" role="dialog" aria-labelledby="contract_modal"
		 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div id="modal_loadable_data">
				<div class="modal-header">
					<h5 class="modal-title" id="contract_modal_title">Арендатор </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					Body
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?=WEB_ROOT?>/public/js/lib/datatables/datatables.min.css"/>

<script type="text/javascript" src="<?=WEB_ROOT?>/public/js/lib/datatables/datatables.min.js"></script>

<script type="text/javascript" src="<?=WEB_ROOT?>/public/js/lib/datepicker_orig.min.js"></script>
<script type="text/javascript" src="<?=WEB_ROOT?>/public/js/lib/jquery.nanogallery2.min.js"></script>
<link href="<?=WEB_ROOT?>/public/css/datepicker.min.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
	$(document).ready(function () {

		let filter = '<?=$_GET["filter"] ?? "" ?>';

		let datatable = $('#owner_contracts').DataTable({
			"order": [ [ 0, 'desc' ] ],
			"language": {
				"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
			},
			"ordering": false,
			"ajax": "/admin/owners/contracts/get_all_ajax?filter=" + filter,
			"columns": [
				{ "data": "id" },
				{ "data": "name" },
				{ "data": "phone" },
				{ "data": "address" },
				{ "data": "status" },
				{ "data": "updated_at" },
			],
		})

		function load_contract_data(contract_id) {
			$.ajax({
				type: "POST",
				url: '/admin/owners/contracts/get_contract_modal_ajax',
				data: {
					contract_id: contract_id,
				},
				dataType: 'json',
				success: function (data) {
					if (data.status != undefined && data.html != undefined) {
						$('#modal_loadable_data').html(data.html)

						$('#contract_modal').modal()

						$('#contract_modal').one('hidden.bs.modal', function () {
							$("#my_nanogallery2").nanogallery2('destroy')
						})
						$('#contract_modal').one('shown.bs.modal', function () {
							$("#my_nanogallery2").nanogallery2({
								thumbnailHeight: 150,
								thumbnailWidth: 150,
								galleryDisplayMode: 'fullContent',
								thumbnailL1BorderHorizontal: 0,
								thumbnailL1BorderVertical: 0,
								thumbnailL1GutterWidth: 10,
								thumbnailL1GutterHeight: 10,
								thumbnailDisplayOutsideScreen: true,
							})
						})
					} else {
						alert('Возникла неизвестная ошибка, см. консоль')
						console.log(data)
					}
					// console.log(data);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Произошла ошибка, попробуйте позже! ' + jqXHR + ' ' + textStatus + ' ' + errorThrown)
					console.log(jqXHR)
					console.log(textStatus)
					console.log(errorThrown)
				}
			})

			return true
		}

		$('#owner_contracts').on('click', 'tbody tr', function () {
			var contract_id = $(this).find('span.contract_id').data('id')
			load_contract_data(contract_id)
		})

		function accept_contract(contract_id) {
			$.ajax({
				type: "POST",
				url: '/admin/owners/contracts/accept_contract_ajax',
				data: {
					contract_id: contract_id,
				},
				dataType: 'json',
				success: function (data) {
					if (data.status == 'ok') {
						alert('Подключение подтверждено, добавлен новый апартамент')
						$('#contract_modal').modal('hide')
						datatable.ajax.reload();

						// load_contract_data(contract_id)
						// datatable.ajax.reload();
					} else {
						alert('Возникла неизвестная ошибка, см. консоль')
						console.log(data)
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Произошла ошибка, попробуйте позже! ' + jqXHR + ' ' + textStatus + ' ' + errorThrown)
					console.log(jqXHR)
					console.log(textStatus)
					console.log(errorThrown)
				}
			})

			return true
		}

		function reject_contract(contract_id) {
			$.ajax({
				type: "POST",
				url: '/admin/owners/contracts/reject_contract_ajax',
				data: {
					contract_id: contract_id,
				},
				dataType: 'json',
				success: function (data) {
					if (data.status == 'ok') {
						alert('Заявка отклонена')
						$('#contract_modal').modal('hide')
						datatable.ajax.reload();

						// load_contract_data(contract_id)
					} else {
						alert('Возникла неизвестная ошибка, см. консоль')
						console.log(data)
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Произошла ошибка, попробуйте позже! ' + jqXHR + ' ' + textStatus + ' ' + errorThrown)
					console.log(jqXHR)
					console.log(textStatus)
					console.log(errorThrown)
				}
			})

			return true
		}

		$('#modal_loadable_data').on('click', '#modal_accept_button', function () {
			let contract_id = $(this).data('id')
			accept_contract(contract_id)
		})

		$('#modal_loadable_data').on('click', '#modal_reject_button', function () {
			let contract_id = $(this).data('id')
			reject_contract(contract_id)
		})

		$('.js_filter_radio').click(function() {
			let filterName = $(this).data('filter');
			datatable.ajax.url("/admin/owners/contracts/get_all_ajax?filter=" + filterName);
			datatable.ajax.reload();

			history.pushState({}, '', '?filter=' + filterName)

		})
	})

</script>
