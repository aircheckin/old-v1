<?php
function get_owner_contract_status_title($status)
{
	switch ($status) {
		case 1:
			return 'Создана';
		case 2:
			return 'Ожидает подтверждения';
		case 3:
			return 'Отклонена';
		case 4:
			return 'Заблокирована';
		case 5:
			return 'Закрыта';
		case 9:
			return 'Подтверждена';
		default:
			return 'Неизвестен';
	}
}

function get_owner_contract_images($contract_id) {
	$contract_images_folder = $contract_id . '/images/';
	$files_uri = OWNERS_CONTRACTS_FILES_URI . $contract_images_folder;
	$files_dir = OWNERS_CONTRACTS_FILES_DIR . $contract_images_folder;

	$files = glob($files_dir . '*.*');

	if (!empty($files)) {
		$files = array_map(function ($local_path) use ($files_uri) {
			$file_name = basename($local_path);
			$web_path = WEB_ROOT . $files_uri . $file_name;
			$thumb_web_path = WEB_ROOT . $files_uri . 'thumbs/' . $file_name;

			return ['full' => $web_path, 'thumb' => $thumb_web_path];
		}, $files);
	}

	return $files;
}
